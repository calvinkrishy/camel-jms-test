package com.example.test;

import org.apache.camel.builder.RouteBuilder;

import java.net.URLEncoder;

public class FromQueue extends RouteBuilder {
    @Override
    public void configure() throws Exception {

        from("activemq:queue:Consumer.A.VirtualTopic.orders?selector=" + URLEncoder.encode("id > 5", "UTF-8"))
                .unmarshal()
                .json()
                .bean(new OrderReceiverBean());

    }
}
