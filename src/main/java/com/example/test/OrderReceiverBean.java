package com.example.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrderReceiverBean {

    Logger logger = LoggerFactory.getLogger(getClass());

    public void process(OrderBean orderBean){
        logger.info(orderBean.toString());
    }
}
