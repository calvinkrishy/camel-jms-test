package com.example.test;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;

import java.util.Random;
import java.util.UUID;

public class ToDestination extends RouteBuilder{

    @Override
    public void configure() throws Exception {
        from("timer://sender?repeatCount=1000")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        Message out = exchange.getOut();
                        out.setHeader("id", random.nextInt(10));
                        out.setBody(get());
                    }
                })
                .log("${property.CamelTimerCounter}# ${header.id}: ${body}")
                .marshal()
                .json()
                .to("activemq:topic:VirtualTopic.orders");
    }

    Random random = new Random();
    private OrderBean get(){
        return new OrderBean(random.nextInt(100), UUID.randomUUID().toString());
    }
}
